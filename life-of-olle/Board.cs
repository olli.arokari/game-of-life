﻿namespace life_of_olle
{
    internal class Board
    {
        private List<Cell> cells = new();
        public int Width { get; set; }
        public int Height { get; set; }

        public Board(int width, int height)
        {
            Width = width;
            Height = height;
            for (int i = 0; i < width * height; i++)
            {
                cells.Add(new Cell());
            }
        }

        public Board Copy()
        {
            Board newBoard = new(Width, Height)
            {
                cells = cells.ConvertAll(x => new Cell(x.Alive)) // deep copy
            };
            return newBoard;
        }

        public Cell GetCell(int x, int y)
        {
            return cells[y * Width + x];
        }

        public void Kill(int x, int y)
        {
            GetCell(x, y).Alive = false;
        }

        public void Resurrect(int x, int y)
        {
            GetCell(x, y).Alive = true;
        }

        public int GetLiveNeighbours(int x, int y)
        {
            int liveNeighbours = 0;
            for (int xx = x - 1; xx <= x + 1; xx++)
            {
                if (xx < 0 || xx >= Width) { continue; }
                for (int yy = y - 1; yy <= y + 1; yy++)
                {
                    if (yy < 0 || yy >= Height || (xx == x && yy == y)) { continue; } // Don't include the cell itself, only its neighbours
                    if (GetCell(xx, yy).Alive) { liveNeighbours++; }
                }
            }
            return liveNeighbours;
        }
    }
}
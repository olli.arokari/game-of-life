﻿namespace life_of_olle
{
    internal static class View
    {
        public static void Show(Board board)
        {
            for (int y = board.Height - 1; y >= 0; y--)
            {
                Console.SetCursorPosition(0, y);
                string row = "";
                for (int x = 0; x < board.Width; x++)
                {
                    if (board.GetCell(x, y).Alive)
                    {
                        row += "█";
                    }
                    else
                    {
                        row += " ";
                    }
                }
                Console.Write(row);
            }
            Console.SetCursorPosition(0, 0); // keep the cursor in the top left corner, so it doesn't jump around
        }
    }
}
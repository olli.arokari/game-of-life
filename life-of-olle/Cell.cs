﻿namespace life_of_olle
{
    internal class Cell
    {
        private static readonly Random r = new();
        public bool Alive { get; set; }

        public Cell()
        {
            Alive = r.NextDouble() > 0.5;
        }

        public Cell(bool alive)
        {
            Alive = alive;
        }
    }
}
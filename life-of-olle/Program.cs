﻿using System.Diagnostics;

namespace life_of_olle
{
    internal class Program
    {
        private static int board_height = 50;
        private static int board_width = 25;
        private static int fps = 20;

        private static void Main(string[] args)
        {
            // Args, width height fps
            try
            {
                board_width = int.Parse(args[0]);
                board_height = int.Parse(args[1]);
                fps = int.Parse(args[2]);
            }
            catch { }

            // Prepare
            Console.Clear();
            Board board = new Board(board_height, board_width);
            View.Show(board);

            // "FPS correction" by measuring the speed it takes to run one generation. The measurement is only taken once
            bool speedtested = false;
            int sleeptime = 100;
            Stopwatch sw = new();

            // Game loop
            while (true)
            {
                if (!speedtested) { sw.Start(); }
                Board lastGen = board.Copy();

                for (int x = 0; x < lastGen.Width; x++)
                {
                    for (int y = 0; y < lastGen.Height; y++)
                    {
                        int neighbours = lastGen.GetLiveNeighbours(x, y);
                        if (neighbours > 8)
                        {
                            throw new Exception("impossible amount of neighbours");
                        }
                        Cell currentCell = lastGen.GetCell(x, y);
                        if (currentCell.Alive && (neighbours != 2 && neighbours != 3))
                        {
                            board.Kill(x, y);
                        }
                        if (!currentCell.Alive && neighbours == 3)
                        {
                            board.Resurrect(x, y);
                        }
                    }
                }

                View.Show(board);
                if (!speedtested)
                {
                    sw.Stop();
                    sleeptime = (int)Math.Round(1000.0 / fps, 0) - (int)sw.ElapsedMilliseconds;
                    if (sleeptime < 0) sleeptime = 0; // The system is too slow to run the requested FPS, so it just runs as fast as it can
                    speedtested = true;
                }
                Thread.Sleep(sleeptime);
            }
        }
    }
}